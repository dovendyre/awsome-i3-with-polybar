#!/bin/sh

echo " "    
echo "#############################################################"    
echo "This will update my awsome-i3-with-polybar on your computer "    
echo " "    
echo "Please follow instructions... Use at own risk..... "    
echo " "    
echo "#############################################################"    

### Henter ut i hvilken mappe scriptet ligger
scriptdir=$(dirname $(readlink -f $0))


## Configure nemo file manager
gsettings set org.cinnamon.desktop.default-applications.terminal exec kitty
gsettings set org.nemo.preferences default-folder-viewer 'list-view'
gsettings set org.nemo.list-view default-zoom-level standard
gsettings set org.nemo.preferences show-home-icon-toolbar true
gsettings set org.nemo.preferences show-new-folder-icon-toolbar true
gsettings set org.nemo.window-state start-with-menu-bar false

## Changing look and feel
[ -f ~/.config/gtk-3.0/settings.ini ] && mv ~/.config/gtk-3.0/settings.ini ~/.config/gtk-3.0/settings.ini_org
cp -f $scriptdir/gtk/settings.ini ~/.config/gtk-3.0/

## Setting login theme
sudo sed -i "/CursorTheme=/c\CursorTheme=Nordzy-cursors" /etc/sddm.conf

## If picom, conky or i3 config exists, backup them    
[ -f ~/.config/i3/config ] && [ ! -f ~/.config/i3/config_org ] && mv ~/.config/i3/config ~/.config/i3/config_org    
[ -d ~/.config/conky ] && [ ! -d ~/.config/conky ] && mv ~/.config/conky ~/.config/conky_org    
[ -f ~/.config/i3/picom.conf ] && [ ! -f ~/.config/i3/picom.conf_org ] && mv ~/.config/i3/picom.conf ~/.config/i3/picom.conf_org
[ -f ~/.config/i3/scripts/picom-toggle.sh ] && [ ! -f ~/.config/i3/scripts/picom-toggle.sh_org ] && mv ~/.config/i3/scripts/picom-toggle.sh ~/.config/i3/scripts/picom-toggle.sh_org
[ -f ~/.config/i3/scripts/conky-toggle.sh ] && [ ! -f ~/.config/i3/scripts/conky-toggle.sh_org ] && mv ~/.config/i3/scripts/conky-toggle.sh ~/.config/i3/scripts/conky-toggle.sh_org
[ -d ~/.config/jgmenu ] && [ ! -d ~/.config/jgmenu_org ] && mv ~/.config/jgmenu ~/.config/jgmenu_org

## Backups existing scripts, i3blocks and archlinux-logout
[ -d ~/.config/archlinux-logout ] && [ ! -d ~/.config/archlinux-logout_org ] && mv ~/.config/archlinux-logout ~/.config/archlinux-logout_org
[ -d ~/.config/polybar ] && [ ! -d ~/.config/polybar_org ] && mv ~/.config/polybar ~/.config/polybar_org

## Replace the mimeapps.list to set default apps 
cp -f -R $scriptdir/mime/* ~/.config/

## "Ads new conky config"
cp -f -R $scriptdir/conky ~/.config/

## Adding jgmenu config    
cp -f -R $scriptdir/jgmenu ~/.config/
                                
## "Ads new picom and i3 config"
cp -f -R $scriptdir/i3/* ~/.config/i3/

## Ads new archlinux-logout config
cp -f -R $scriptdir/archlinux-logout ~/.config/

## Ads some new script folder and put some scripts in it
[ ! -d ~/.config/dovenscripts ] && mkdir ~/.config/dovenscripts
cp -f -R $scriptdir/script/* ~/.config/dovenscripts/

## Using a laptop and want to lock screen when you close the lid?            
echo " "            
echo "###########################################################"            
echo -n "Using a laptop and want to lock screen when you close the lid? (y/n) "            
echo " "            
echo "###########################################################"            
old_stty_cfg=$(stty -g)            
stty raw -echo ; answer=$(head -c 1) ; stty $old_stty_cfg # Careful playing with stty            
if echo "$answer" | grep -iq "^y" ;then    
## Makes computer lock with suspend or lid close    
 [ ! -f /etc/systemd/system/i3lock.service ] && sudo cp -f -R $scriptdir/systemd/i3lock.service /etc/systemd/system/    
                     
nonerootuser=$USER    
sudo sed -i "/User=/c\User=$nonerootuser" /etc/systemd/system/i3lock.service    
sudo sed -i "/ExecStart=/c\ExecStart=/home/$nonerootuser/.config/dovenscripts/lock.sh" /etc/systemd/system/i3lock.service    
sudo systemctl enable i3lock.service    
fi 

## Ads new polybar config
cp -f -R $scriptdir/polybar ~/.config/

## Checks if rofi exists and backups the existing
[ -d ~/.config/rofi ] && [ ! -d ~/.config/rofi_org ] && mv ~/.config/rofi ~/.config/rofi_org

## Ads the new rofi config
cp -f -R $scriptdir/rofi ~/.config/

## Ads pywal colors to zsh setup
cp -f -R $scriptdir/config/.zshrc-pywal ~/

## Change text to correct keybinding on arcologout
#[ -f /usr/share/arcologout/GUI.py ] && sudo sed -i 's/(L)/(O)/g' /usr/share/arcologout/GUI.py
#[ -f /usr/share/arcologout/GUI.py ] && sudo sed -i 's/(K)/(L)/g' /usr/share/arcologout/GUI.py

## Backup the original kitty config
[ -f ~/.config/kitty/kitty.conf ] && [ ! -f ~/.config/kitty/kitty.conf_org ] && mv ~/.config/kitty/kitty.conf ~/.config/kitty/kitty.conf_org

## Updateing the kitty config
[ ! -d ~/.config/kitty ] && mkdir ~/.config/kitty
cp -f -R $scriptdir/kitty/* ~/.config/kitty/

## Makes users icon and themes folder
[ ! -d ~/.icons ] && mkdir ~/.icons
[ ! -d ~/.themes ] && mkdir ~/.themes

## Adds the mouse pointer as default cursor theme
cp -f -R $scriptdir/config/default ~/.icons/

## Add the new themes icons and pointers
sudo cp -f -R $scriptdir/icons/* /usr/share/icons/
sudo cp -f -R $scriptdir/themes/* /usr/share/themes/

## Font made global
sudo cp -f -R $scriptdir/fonts/* /usr/share/fonts/

## Mouse corsur for the login screen
sudo cp -f -R $scriptdir/mouse/* /usr/share/icons/

## Adding wallpapers to home dir
[ ! -d ~/.wallpapers ] && mkdir ~/.wallpapers
cp -f -R /$scriptdir/wallpapers/* ~/.wallpapers/

echo " "
echo " "
echo " ######### "
echo " D O N E ! "
echo " ######### "
echo " "
echo " "
echo " ###################################################################################"
echo " To disable locking screen at suspend run command: systemctl disable i3lock.service "
echo " ###########################################################################################"
echo " Add your favourute wallpapers to the ~/.wallpapers directory. Will be randomized at login. "
echo " ###########################################################################################"
echo " "
echo " ################################################################################"
echo " If you use Arcolinux and have a multi monitor setup uncomment the following line in ~/.config/i3/config"
echo " exec --no-startup-id xrandr --output DP-2 --right-of DP-1 --auto "
echo " Rember to change --output DP-2 --right-of DP-1 --auto to match your config "
echo " #################################################################################################################################################"
echo " "
echo " Please reboot for all changes to take effect! "
