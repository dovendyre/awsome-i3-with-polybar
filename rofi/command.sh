#!/usr/bin/env bash

theme="doven_style"
dir="$HOME/.config/rofi"

rofi -no-lazy-grab -show run -modi run -theme $dir/"$theme"
