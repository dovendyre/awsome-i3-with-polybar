#!/usr/bin/env bash

theme="doven_style"
dir="$HOME/.config/rofi"

rofi -no-lazy-grab -show drun -modi drun -theme $dir/"$theme"
