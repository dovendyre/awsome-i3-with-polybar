#!/usr/bin/env bash

theme="doven_style"
dir="$HOME/.config/rofi"

rofi -no-lazy-grab -show window -window-format {t:20} -theme $dir/"$theme"
