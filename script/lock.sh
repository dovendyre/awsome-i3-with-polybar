#!/bin/sh
# Locks screen with betterlockscreen
#
# ##########################################################
# DovendyrE
#
/usr/bin/betterlockscreen -l dim -- --time-str="%H:%M" &
