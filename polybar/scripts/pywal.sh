#!/usr/bin/env bash

# Color files
PFILE="$HOME/.config/polybar/colors.ini"
RFILE="$HOME/.config/rofi/colors.rasi"
CFILE="$HOME/.config/conky/conky-shcts"
JGFILE="$HOME/.config/jgmenu/jgmenurc"

# Get colors
pywal_get() {
	wal -i "$1" -q -t
}

# Change colors
change_color() {
	# polybar
	sed -i -e "s/background = #.*/background = #$BGPOLY/g" $PFILE
	sed -i -e "s/foreground = #.*/foreground = $FG/g" $PFILE
	sed -i -e "s/foreground-alt = #.*/foreground-alt = $FGA/g" $PFILE
	sed -i -e "s/alert = #.*/alert = $SH2/g" $PFILE

	sed -i -e "s/shade1 = #.*/shade1 = $SH1/g" $PFILE
	sed -i -e "s/shade2 = #.*/shade2 = $SH2/g" $PFILE
	sed -i -e "s/shade3 = #.*/shade3 = $SH3/g" $PFILE
	sed -i -e "s/shade4 = #.*/shade4 = $SH4/g" $PFILE
	sed -i -e "s/shade5 = #.*/shade5 = $SH5/g" $PFILE
	sed -i -e "s/shade6 = #.*/shade6 = $SH6/g" $PFILE
	
	# rofi
	cat > $RFILE <<- EOF
               /* colors */    
    
        * {    
          al:  #00000000;
          bg:  ${BG}55;
          se:  ${SH3}FF;
          fg:  ${FG}FF;
          ac:  ${SH2}FF;
        }
	EOF

	# Conky
	sed -i -e "s/own_window_colour = '#.*/own_window_colour = '#$BGPOLY',/g" $CFILE
	sed -i -e "s/default_color = '#.*/default_color = '$FG',/g" $CFILE
	sed -i -e "s/color2 = '#.*/color2 = '$SH3',/g" $CFILE
	sed -i -e "s/color1 = '#.*/color1 = '$SH2',/g" $CFILE
	
	# Jgmenu
	sed -i -e "s/color_menu_bg = #.*/color_menu_bg = #$BGPOLY 70/g" $JGFILE
	sed -i -e "s/color_menu_bg_to = #.*/color_menu_bg_to = #$BGPOLY 100/g" $JGFILE
	sed -i -e "s/color_norm_bg = #.*/color_norm_bg = #$BGPOLY 0/g" $JGFILE
	sed -i -e "s/color_norm_fg = #.*/color_norm_fg = $SH3 100/g" $JGFILE
	sed -i -e "s/color_sel_bg = #.*/color_sel_bg = #$BGPOLY 0/g" $JGFILE
	sed -i -e "s/color_sel_fg = #.*/color_sel_fg = $FG 100/g" $JGFILE

	polybar-msg cmd restart
}

# Main
if [[ -f "/usr/bin/wal" ]]; then
	if [[ "$1" ]]; then
		pywal_get "$1"

		# Source the pywal color file
		. "$HOME/.cache/wal/colors.sh"

		BG=`printf "%s\n" "$background"`
		FG=`printf "%s\n" "$foreground"`
		FGA=`printf "%s\n" "$foreground"`
		SH1=`printf "%s\n" "$color1"`
		SH2=`printf "%s\n" "$color2"`
		SH3=`printf "%s\n" "$color3"`
		SH4=`printf "%s\n" "$color4"`
		SH5=`printf "%s\n" "$color5"`
		SH6=`printf "%s\n" "$color6"`
                BGPOLY=${BG:1}

		change_color
	else
		echo -e "[!] Please enter the path to wallpaper. \n"
		echo "Usage : ./pywal.sh path/to/image"
	fi
else
	echo "[!] 'pywal' is not installed."
fi
