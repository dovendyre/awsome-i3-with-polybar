# Awsome i3 with polybar

My Garudalinux rice with i3 and polybar.

Feel free to tailor this for you own needs!

CAUTION! Use at own risk!


![LookAndFeel](https://gitlab.com/dovendyre/awsome-i3-with-polybar/-/raw/main/images/Awsome-i3-with-polybar.gif)

## Installation

* This script assumes that you have Garudalinux with i3 desktop


```
git clone https://gitlab.com/dovendyre/awsome-i3-with-polybar.git && bash awsome-i3-with-polybar/doven_install.sh
```

## Update (Sorry, not working atm....)

```
git clone https://gitlab.com/dovendyre/awsome-i3-with-polybar.git && bash awsome-i3-with-polybar/update_doven.sh
```

## Pywal in apps
* Add extention dlasagno.wal-theme to VSCodium to get pywall theme.


# i3wm keyboard shortcuts
3 changes
 ◦ Fix awsome nvim script path for youcompleteme
 ◦ Add conky cheet sheet
 ◦ Fix i3lock removal to install arcolinux tools
 ◦ Change applaunch from i3config to dex
 ◦ Fix update tool
 ◦ Get themes and icons from creators git (keep them up to date)


I3 Shortcuts
sorry... know its messy... but getting better...

Ctrl + Alt + Right = Move to the next workspace
Ctrl + Alt + Left = Move to the previous workspace
Mod = Holding it down allows you to grab and move floating windows
Mod + f = Toggle fullscreen for the focused container
Ctrl + Shift + Esc = Open taskmanager (xfce4-taskmanager)
Mod + b = Toggle border control
Ctrl + Alt + o = Toggle picom on and off (Transparency)

| Shortcut                      | Description                                       |
| ----------------------------- | ------------------------------------------------- |
| *modkey*                      |  /   (tuxkey / winkey)                        |
| *modkey + 1-9*                | Change workspace                                  |
| *modkey + Shift + 1-9*        | Move current window to selected workspace         |
| *modkey + d*                  | Rofi App launcher                                 |
| *modkey + z*                  | Xfce4-menu                                        |
| *modkey + Tab*                | List open apps (Rofi)                             |
| *modkey + (arrow right)*      | Move focus to the right window                    |
| *modkey + (arrow left)*       | Move focus to the left window                     |
| *modkey + (arrow down)*       | Move focus to the down window                     |
| *modkey + Shift + (arrow up)* | Move focus to the up window                       |
| *modkey + Shift + Space*      | Toggle focus between floating and tiled windows   |
| *modkey + w*                  | Tabbed window mode (current workspace)            |
| *modkey + s*                  | Stacked window mode (current workspace)           |
| *modkey + e*                  | Tiled window mode (current workspace)             |
| *modkey + Enter*              | Open Terminal (Kitty)                             |
| *modkey + F1*                 | Open Brave Browser                                |
| *modkey + F3*                 | Open Nemo file manager                            |
| *Ctrl + Alt + p*              | Add / Remove programs (pamac)                     |
| *Ctrl + Alt + m*              | Open pulseaudio control panel                     |
| *modkey + Shift + r*          | Restart i3 inplace                                |
| *modkey + Shift + c*          | Reload i3                                         |
| *Alt + Tab*                   | Cycle workspaces                                  |
| *Alt + Shift + Tab*           | Cycle workspaces reverse                          |
| **        |       |

# Thanks!

## Arcolinux
https://arcolinux.com/

## Garudalinux
https://garudalinux.org/

## KORA icons
https://github.com/bikass/kora

## Jasper gtk theme
https://github.com/vinceliuice/Jasper-gtk-theme

## Polybar
https://polybar.github.io/

## Graphite cursors
https://github.com/vinceliuice/Graphite-cursors
