#!/bin/bash
if pgrep -x "conky" > /dev/null
then
	killall conky
        echo 'Hidden=true' >> ~/.config/autostart/conky.desktop
else
        sed -i -e '/Hidden=true/d' ~/.config/autostart/conky.desktop
        conky --daemonize
fi