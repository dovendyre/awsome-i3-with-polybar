#!/usr/bin/env bash

# Color files
PFILE="$HOME/.config/polybar/colors.ini"
RFILE="$HOME/.config/rofi/colors.rasi"
CFILE="$HOME/.config/conky/conky.conf"
JGFILE="$HOME/.config/jgmenu/jgmenurc"
DUFILE="$HOME/.config/dunst/dunstrc"

# Get colors
pywal_get() {
	wal -i "$1" -q -t
}

# Change colors
change_color() {
	# polybar
	sed -i -e "s/background = #.*/background = #90$BGPOLY/g" $PFILE
	sed -i -e "s/foreground = #.*/foreground = $FG/g" $PFILE
	sed -i -e "s/foreground-alt = #.*/foreground-alt = $FGA/g" $PFILE
	sed -i -e "s/alert = #.*/alert = $SH6/g" $PFILE

	sed -i -e "s/shade1 = #.*/shade1 = $SH1/g" $PFILE
	sed -i -e "s/shade2 = #.*/shade2 = $SH2/g" $PFILE
	sed -i -e "s/shade3 = #.*/shade3 = $SH3/g" $PFILE
	sed -i -e "s/shade4 = #.*/shade4 = $SH4/g" $PFILE
	sed -i -e "s/shade5 = #.*/shade5 = $SH5/g" $PFILE
	sed -i -e "s/shade6 = #.*/shade6 = $SH6/g" $PFILE
	
	# rofi
	cat > $RFILE <<- EOF
               /* colors */    
    
        * {    
          al:  #00000000;
          bg:  ${BG}55;
          se:  ${SH2}FF;
          fg:  ${FG}FF;
          ac:  ${SH6}FF;
        }
	EOF

	# Conky

	killall conky

	sed -i -e "s/own_window_colour = '#.*/own_window_colour = '#$BGPOLY',/g" $CFILE
	sed -i -e "s/default_color = '#.*/default_color = '$SH6',/g" $CFILE
	sed -i -e "s/color2 = '#.*/color2 = '$FG',/g" $CFILE
	sed -i -e "s/color1 = '#.*/color1 = '$SH2',/g" $CFILE

	if ! grep "Hidden=true" ~/.config/autostart/conky.desktop
	then
  		conky --daemonize --pause=1
 	fi
	
	# Jgmenu
	sed -i -e "s/color_menu_bg = #.*/color_menu_bg = #$BGPOLY 70/g" $JGFILE
	sed -i -e "s/color_menu_bg_to = #.*/color_menu_bg_to = #$BGPOLY 100/g" $JGFILE
	sed -i -e "s/color_norm_bg = #.*/color_norm_bg = #$BGPOLY 0/g" $JGFILE
	sed -i -e "s/color_norm_fg = #.*/color_norm_fg = $SH6 100/g" $JGFILE
	sed -i -e "s/color_sel_bg = #.*/color_sel_bg = #$BGPOLY 0/g" $JGFILE
	sed -i -e "s/color_sel_fg = #.*/color_sel_fg = $FG 100/g" $JGFILE

	# Dunst
	if pidof dunst > /dev/null
	then
		killall -q dunst
	fi
	sed -i -e "s/background =.*/background = \"#$BGPOLY\"/g" $DUFILE
	sed -i -e "s/foreground =.*/foreground = \"$FG\"/g" $DUFILE
	sed -i -e "s/frame_color =.*/frame_color = \"$SH2\"/g" $DUFILE
	dunst -config ~/.config/dunst/dunstrc

	#polybar-msg cmd restart
}

# Main
if [[ -f "/usr/bin/wal" ]]; then
	if [[ "$1" ]]; then
		pywal_get "$1"

		# Source the pywal color file
		. "$HOME/.cache/wal/colors.sh"

		BG=`printf "%s\n" "$background"`
		FG=`printf "%s\n" "$foreground"`
		FGA=`printf "%s\n" "$foreground"`
		SH1=`printf "%s\n" "$color1"`
		SH2=`printf "%s\n" "$color2"`
		SH3=`printf "%s\n" "$color3"`
		SH4=`printf "%s\n" "$color4"`
		SH5=`printf "%s\n" "$color5"`
		SH6=`printf "%s\n" "$color6"`
                BGPOLY=${BG:1}

		change_color
	else
		echo -e "[!] Please enter the path to wallpaper. \n"
		echo "Usage : ./pywal.sh path/to/image"
	fi
else
	echo "[!] 'pywal' is not installed."
fi
