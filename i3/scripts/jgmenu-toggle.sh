#!/bin/bash
if grep "empty-space jgmenu empty-space i3 spacee xwindow spacee" ~/.config/polybar/config.ini
then
        sed -i -e "s/modules-left = empty-space jgmenu empty-space i3 spacee xwindow spacee/modules-left = empty-space i3 spacee xwindow spacee/g" ~/.config/polybar/config.ini
else
        sed -i -e "s/modules-left = empty-space i3 spacee xwindow spacee/modules-left = empty-space jgmenu empty-space i3 spacee xwindow spacee/g" ~/.config/polybar/config.ini
fi
