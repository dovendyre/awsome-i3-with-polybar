################# Start of all the settings

# KEY DEFINITIONS TO REMEMBER
# $mod = Set below
# Mod4 = Super key
# Mod1 = ALT key
# Control = CTRL key
# Shift = SHIFT key
# Escape = ESCAPE key
# Return = ENTER or RETURN key
# KP_Enter = Keypad Enter
# Pause = PAUSE key
# Print = PRINT key
# Tab = TAB key

########
# Font #
########
# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
# choose your font
font pango:Cousine Nerd Font 9

################################
# Define the $mod variable/key #
################################

# Key to rule them all : Super(Windows) or Alt key - see definition above
# set Super key
set $mod Mod4

#####################
# workspace layout: #
#####################

# default i3 tiling mode:
workspace_layout default

# i3 stacking layout:
# Each window will be fullscreen and tabbed top to bottom.
#workspace_layout stacking

# i3 tabbed layout:
# Each new window will open fullscreen as a tab (left to right)
#workspace_layout tabbed

#########################
# Default Applications: #
#########################
    
set $preferredTerminalEmulator /usr/bin/kitty
set $preferredBrowser /usr/bin/brave
set $preferredFileManager /usr/bin/nemo    
set $preferredMailClient /usr/bin/geary    
set $preferredTextEditor /usr/bin/vscodium

#########################################
# Define the movements keys - variables #
#########################################

# This is setup the same as vim 
set $up k
set $down j
set $left h
set $right l 

################################################
# Single and Dual screen                       #
# Make your adjustments                        #
# Workspace 8 and 9 defaults to second monitor #
################################################

##################################################################
# Using Garuda Linux? Set up the screens in the GUI application! #
##################################################################

# current displays
#exec --no-startup-id xrandr --output DP-2 --right-of DP-1 --auto

# setting variables for later use
# use xrandr and/or arandr to know the names of your monitors
# use this line to tell which monitor is on the right
# xrandr --output DVI-I-2 --right-of DVI-I-1 --auto
# exec --no-startup-id xrandr --output LVDS1 --mode 1366x768 --output DP3 --mode 1920x1080 --right-of LVDS1
# exec --no-startup-id xrandr --output DVI-I-0 --right-of HDMI-0 --auto
# exec --no-startup-id xrandr --output DVI-1 --right-of DVI-0 --auto
# exec --no-startup-id xrandr --output DVI-D-1 --right-of DVI-I-1 --auto
# exec --no-startup-id xrandr --output HDMI-2 --right-of HDMI-1 --auto

# current setup
#set $firstMonitor DP-1
#set $secondMonitor DP-2

#set $firstMonitor eDP

#set $firstMonitor HDMI-0
#set $secondMonitor DP0

# Other Examples

#set $firstMonitor DP3
#set $secondMonitor LVDS1

#set $firstMonitor DVI-I-0
#set $secondMonitor HDMI-0

#set $firstMonitor DVI-0
#set $secondMonitor DVI-1

#set $firstMonitor DVI-I-1
#set $secondMonitor DVI-D-1

#set $firstMonitor HDMI-1
#set $secondMonitor HDMI-2

workspace $ws1 output $firstMonitor
workspace $ws2 output $firstMonitor
workspace $ws3 output $firstMonitor
workspace $ws4 output $firstMonitor
workspace $ws5 output $firstMonitor
workspace $ws6 output $firstMonitor
workspace $ws7 output $firstMonitor
workspace $ws8 output $secondMonitor
workspace $ws9 output $secondMonitor

# switch to workspace
bindsym $mod+1  workspace  $ws1
bindsym $mod+2  workspace  $ws2
bindsym $mod+3  workspace  $ws3
bindsym $mod+4  workspace  $ws4
bindsym $mod+5  workspace  $ws5
bindsym $mod+6  workspace  $ws6
bindsym $mod+7  workspace  $ws7
bindsym $mod+8  workspace  $ws8
bindsym $mod+9  workspace  $ws9

# switch to workspace with numpad keys
bindcode $mod+87 workspace $ws1
bindcode $mod+88 workspace $ws2
bindcode $mod+89 workspace $ws3
bindcode $mod+83 workspace $ws4
bindcode $mod+84 workspace $ws5
bindcode $mod+85 workspace $ws6
bindcode $mod+79 workspace $ws7
bindcode $mod+80 workspace $ws8
bindcode $mod+81 workspace $ws9

# switch to workspace with numlock numpad keys
bindcode $mod+Mod2+87 workspace $ws1
bindcode $mod+Mod2+88 workspace $ws2
bindcode $mod+Mod2+89 workspace $ws3
bindcode $mod+Mod2+83 workspace $ws4
bindcode $mod+Mod2+84 workspace $ws5
bindcode $mod+Mod2+85 workspace $ws6
bindcode $mod+Mod2+79 workspace $ws7
bindcode $mod+Mod2+80 workspace $ws8
bindcode $mod+Mod2+81 workspace $ws9

# move focused container to workspace
bindsym $mod+Shift+1    move container to workspace  $ws1; workspace  $ws1
bindsym $mod+Shift+2    move container to workspace  $ws2; workspace  $ws2
bindsym $mod+Shift+3    move container to workspace  $ws3; workspace  $ws3
bindsym $mod+Shift+4    move container to workspace  $ws4; workspace  $ws4
bindsym $mod+Shift+5    move container to workspace  $ws5; workspace  $ws5
bindsym $mod+Shift+6    move container to workspace  $ws6; workspace  $ws6
bindsym $mod+Shift+7    move container to workspace  $ws7; workspace  $ws7
bindsym $mod+Shift+8    move container to workspace  $ws8; workspace  $ws8
bindsym $mod+Shift+9    move container to workspace  $ws9; workspace  $ws9

# move focused container to workspace with numpad keys
bindcode $mod+Shift+Mod2+87 	move container to workspace  $ws1; workspace  $ws1
bindcode $mod+Shift+Mod2+88 	move container to workspace  $ws2; workspace  $ws2
bindcode $mod+Shift+Mod2+89 	move container to workspace  $ws3; workspace  $ws3
bindcode $mod+Shift+Mod2+83 	move container to workspace  $ws4; workspace  $ws4
bindcode $mod+Shift+Mod2+84 	move container to workspace  $ws5; workspace  $ws5
bindcode $mod+Shift+Mod2+85 	move container to workspace  $ws6; workspace  $ws6
bindcode $mod+Shift+Mod2+79 	move container to workspace  $ws7; workspace  $ws7
bindcode $mod+Shift+Mod2+80 	move container to workspace  $ws8; workspace  $ws8
bindcode $mod+Shift+Mod2+81 	move container to workspace  $ws9; workspace  $ws9

# move focused container to workspace with numpad keys
bindcode $mod+Shift+87 	 move container to workspace  $ws1; workspace  $ws1
bindcode $mod+Shift+88 	 move container to workspace  $ws2; workspace  $ws2
bindcode $mod+Shift+89 	 move container to workspace  $ws3; workspace  $ws3
bindcode $mod+Shift+83 	 move container to workspace  $ws4; workspace  $ws4
bindcode $mod+Shift+84 	 move container to workspace  $ws5; workspace  $ws5
bindcode $mod+Shift+85 	 move container to workspace  $ws6; workspace  $ws6
bindcode $mod+Shift+79 	 move container to workspace  $ws7; workspace  $ws7
bindcode $mod+Shift+80 	 move container to workspace  $ws8; workspace  $ws8
bindcode $mod+Shift+81 	 move container to workspace  $ws9; workspace  $ws9

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"

##########
# Menu's #
##########

# start xfce-appfinder
bindsym $mod+z exec --no-startup-id xfce4-appfinder

# Start rofi
bindsym $mod+d exec ~/.config/rofi/launcher.sh

# Rofi windows slector        
bindsym $mod+Tab exec ~/.config/rofi/window.sh    
    
# Rofi run command    
bindsym mod1+F2 exec ~/.config/rofi/command.sh

#################################
# how to exit, logoff, suspend, #
#################################

# ArcoLinux Logout
bindsym $mod+0 exec --no-startup-id archlinux-logout
bindsym control+mod1+x exec --no-startup-id archlinux-logout

################################
# reload changed configuration #
################################

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# reload the configuration file
bindsym $mod+Shift+c reload

###########################
# Stopping an application #
###########################

# kill focused window
bindsym $mod+Shift+q kill
bindsym $mod+q kill

########################
# Moving around in i3  #
########################

#Disables mouse focus
focus_follows_mouse no

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# change focus
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

############################
# moving around workspaces #
############################

# next/previous workspace
bindsym Mod1+Tab workspace next
bindsym Mod1+Shift+Tab workspace prev

# navigate workspaces next / previous
bindsym Mod1+Ctrl+Right workspace next
bindsym Mod1+Ctrl+Left workspace prev

# switch to workspace with urgent window automatically
#for_window [urgent=latest] focus

#####################
# Tiling parameters #
#####################

# orientation for new workspaces
#default_orientation horizontal

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
# qwerty/azerty issue for letter z
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# change focus between tiling / floating windows
bindsym $mod+Shift+space focus mode_toggle

# toggle tiling / floating
bindsym $mod+space floating toggle

# focus the parent container
bindsym $mod+a focus parent

##########
# resize #
##########

# Resizing by 1
bindsym $mod+Ctrl+Right resize shrink width 1 px or 1 ppt
bindsym $mod+Ctrl+Up resize grow height 1 px or 1 ppt
bindsym $mod+Ctrl+Down resize shrink height 1 px or 1 ppt
bindsym $mod+Ctrl+Left resize grow width 1 px or 1 ppt

#####################################
# Assign applications to workspaces #
#####################################

# Assign application to start on a specific workspace
# you can find the class with the program xprop

# Make browser start on workspace 1
assign [class="firefox|Firefox|Chromium|Brave|$preferredBrowser-browser|hiri|Pale moon"]                    → $ws1
assign [class="Chrome|Google Chrome|Google-chrome|Vivaldi-stable|Vivaldi-snapshot|Opera"]       → $ws1

# Workspace 2 text editor related
assign [class="VSCodium|kate|Atom|Joplin|sublime-text|sublime_text|Sublime_text|subl|Subl|subl3|Subl3"]       → $ws2
assign [class="Xed|xed|Brackets|Code|Geany"]                                                    → $ws2

# Workspace 3 file managers
assign [class="Nemo|Thunar|Caja|nautilus|Nautilus|Pcmanfm"]                                     → $ws3

# Workspace 4 Images
# assign [class="ristretto|Ristretto|shotwell|Shotwell|Xviewer|Nitrogen"]                       → $ws4
# assign [class="feh|gthumb|Gthumb|eog|Eog|Pinta|pixeluvo|Pixeluvo"]                            → $ws4
# assign [class="Meld"]                                                                         → $ws4

# Workspace 5 virtual machines and RDP
assign [class="Vmplayer|VirtualBox|org.remmina.Remmina"]					→ $ws5

# Workspace 6 social
assign [class="telegram-desktop|discord|TeamSpeak"]                                                              → $ws6

# Workspace 7 gaming
assign [class="Lutris|Steam|Remmina|gw2.exe|gw2-64.exe"]                                        → $ws7

# Workspace 8 music related
assign [class="Spotify|spotify|Pragha"]                                                         → $ws8
# fix for spotify not moving to workspace 8
# for_window [class="Spotify"] move to workspace 8

# Workspace 9 all system apps
assign [class="Pamac-manager"]                                                                  → $ws9

# Workspace 9 email clients
# assign [class="Geary|Evolution"]                                                              → $ws9

#############
# Autostart #
#############

# USER APPLICATIONS TO START AT BOOT

# Autotiling
exec_always --no-startup-id autotiling

# TRAY APPLICATIONS

# applications that are not installed will not start
# you may see a wheel - hashtag out things you do not want/need

# Authentication dialog
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# num lock activated
exec_always --no-startup-id numlockx on

# Toggle conky
bindsym $mod+c exec --no-startup-id ~/.config/i3/scripts/conky-toggle.sh

# Toggle Jgmenu
bindsym $mod+m exec --no-startup-id ~/.config/i3/scripts/jgmenu-toggle.sh

## Nextcloud sync client autostart
#exec --no-startup-id nextcloud

## Enable Corsair keyboard/mouse software at startup
#exec --no-startup-id ckb-next -b

## Enable GreenWithEnvy at startup
#exec --no-startup-id gwe --hide-window

## Starting dex witch starts .desktop applications from /etc/xdg/autostart and ~/.config/autostart
exec --no-startup-id dex -a -s ~/.config/autostart/:/etc/xdg/autostart/

#######################
# system applications #
#######################

#exec --no-startup-id xfce4-power-manager &
# committed next line to get super key to bring up the menu in xfce and avoid error then in i3
# IF xfsettingsd is activated you can not change themes
# exec --no-startup-id xfsettingsd &
#exec --no-startup-id /usr/lib/xfce4/notifyd/xfce4-notifyd &

## Lauching dunst in pywal.sh -- dont uncomment
#exec_always dunst -config ~/.config/dunst/dunstrc

###################################
# Applications keyboard shortcuts #
###################################

# not workspace related

# terminal
bindsym $mod+Return exec --no-startup-id $preferredTerminalEmulator;focus
bindsym $mod+KP_Enter exec --no-startup-id $preferredTerminalEmulator;focus
bindsym control+mod1+t exec --no-startup-id $preferredTerminalEmulator; focus
bindsym control+mod1+Return exec --no-startup-id $preferredTerminalEmulator; focus
bindsym control+mod1+KP_Enter exec --no-startup-id $preferredTerminalEmulator; focus

# System monitor
bindsym control+Shift+Escape exec --no-startup-id xfce4-taskmanager;focus

# pavucontrol
bindsym $mod+Ctrl+m exec --no-startup-id pavucontrol

# pamac-manager
bindsym control+mod1+p exec --no-startup-id pamac-manager

# xkill
bindsym --release $mod+Escape exec xkill

# Function Keybinds
# Browser
bindsym $mod+F1 exec --no-startup-id $preferredBrowser;focus

# Text Editor
bindsym $mod+F2 exec --no-startup-id $preferredTextEditor;focus

# File management
bindsym $mod+F3 exec --no-startup-id $preferredFileManager

# Mail 
bindsym $mod+F4 exec --no-startup-id $preferredMailClient

# document comparison
bindsym $mod+F5 exec --no-startup-id meld;focus

# virtual machine
bindsym $mod+F7 exec --no-startup-id virtualbox;focus

###############
# screenshots #
###############

bindsym Print exec --no-startup-id scrot -q 100 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'
bindsym $mod+Print exec --no-startup-id flameshot gui

###########################
# floating or tiled rules #
###########################

# floating enabled from some programs - find with xprop

for_window [class="Arcolinux-welcome-app.py"] floating enable
# for_window [class="Arcolinux-tweak-tool.py"] floating enable
for_window [class="Arcolinux-calamares-tool.py"] floating enable
for_window [class="Bleachbit"] floating disable
for_window [class="Blueberry.py"] floating enable
for_window [class="Brasero"] floating disable
for_window [class="Galculator"] floating enable
for_window [class="Gnome-disks"] floating disable
for_window [class="^Gnome-font-viewer$"] floating enable
for_window [class="^Gpick$"] floating enable
for_window [class="Hexchat"] floating disable
for_window [class="Imagewriter"] floating enable
for_window [class="Font-manager"] floating enable
for_window [class="Nitrogen"] floating disable
for_window [class="Pavucontrol"] floating disable
for_window [class="Peek"] floating enable
for_window [class="^Skype$"] floating enable
for_window [class="^Spotify$"] floating disable
for_window [class="System-config-printer.py"] floating enable
for_window [class="Unetbootin.elf"] floating enable
for_window [class="Usb-creator-gtk"] floating enable
for_window [class="^Vlc$"] floating disable
for_window [class="Wine"] floating disable
for_window [class="Xfburn"] floating disable
for_window [class="Xfce4-appfinder"] floating enable
for_window [class="Xfce4-settings-manager"] floating disable
for_window [class="Xfce4-taskmanager"] floating enable

# for_window [instance="gimp"] floating disable
for_window [instance="script-fu"] border normal
for_window [instance="variety"] floating disable

for_window [title="Copying"] floating enable
for_window [title="Deleting"] floating enable
for_window [title="Moving"] floating enable
for_window [title="^Terminator Preferences$"] floating enable

# for_window [window_role="^gimp-toolbox-color-dialog$"] floating enable
for_window [window_role="pop-up"] floating enable
for_window [window_role="Dunst"] floating enable
for_window [window_role="^Preferences$"] floating enable
for_window [window_role="setup"] floating enable
for_window [title="alsamixer"] floating enable border pixel 1    
for_window [class="calamares"] floating enable border normal    
for_window [class="Clipgrab"] floating enable    
for_window [title="File Transfer*"] floating enable    
for_window [class="bauh"] floating enable    
for_window [class="Galculator"] floating enable border pixel 1    
for_window [title="i3_help"] floating enable sticky enable border normal    
for_window [class="Lightdm-settings"] floating enable    
for_window [class="Garuda Settings Manager"] floating enable border normal    
for_window [title="MuseScore: Play Panel"] floating enable    
for_window [class="azote"] floating enable sticky enable border normal    
for_window [class="Oblogout"] fullscreen enable    
for_window [title="About Pale Moon"] floating enable    
for_window [class="Qtconfig-qt4"] floating enable border normal    
for_window [class="qt5ct"] floating enable sticky enable border normal    
for_window [class="Simple-scan"] floating enable border normal    
for_window [class="(?i)System-config-printer.py"] floating enable border normal    
for_window [class="Skype"] floating enable border normal    
for_window [class="Timeshift-gtk"] floating enable border normal    
#for_window [class="(?i)virtualbox"] floating enable border normal    
for_window [class="Xfburn"] floating enable    
for_window [class="keepassxc"] floating enable    
for_window [class="garuda-welcome"] floating enable

# give focus to applications

for_window [class="Gnome-terminal"] focus
for_window [class="Termite"] focus
for_window [class="Terminator"] focus
for_window [class="Urxvt"] focus
for_window [class="kitty"] focus

##################
# audio settings #
##################

# Use pactl to adjust volume in PulseAudio.    
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle

# https://github.com/acrisci/playerctl/
bindsym XF86AudioPlay exec --no-startup-id playerctl play-pause
bindsym XF86AudioNext exec --no-startup-id playerctl next
bindsym XF86AudioPrev exec --no-startup-id playerctl previous
bindsym XF86AudioStop exec --no-startup-id playerctl stop

## Asus Backlight (My Asus rog laptop)
#bindsym XF86MonBrightnessUp exec --no-startup-id light -A 10 # increase screen brightness
#bindsym XF86MonBrightnessDown exec --no-startup-id light -U 10 # decrease screen brightness

## Asus KB light (My Asus rog laptop)
bindsym XF86KbdBrightnessUp exec --no-startup-id asusctl -n # increase keyboard light
bindsym XF86KbdBrightnessDown exec --no-startup-id asusctl -p # decrease keyboard light

## TouchPad toggle (My Asus rog laptop)
bindsym XF86TouchpadToggle exec ~/.config/dovenscripts/toggletouchpad.sh # toggle touchpad

##################
# border control #
##################

# Border control
hide_edge_borders both
bindsym $mod+b exec --no-startup-id i3-msg border toggle

# new_window pixel 1
new_window normal
# new_window none

# new_float pixel 1
new_float normal
# new_float none

###################
# Popups  control #
###################

# Popups during fullscreen mode
popup_during_fullscreen smart

################
# i3 gaps next #
################

# Settings for I3 next gap git
# https://github.com/Airblader/i3/tree/gaps-next
# delete or uncomment the following lines if you do not have it or do not
# want it

for_window [class="^.*"] border pixel 2
gaps inner 2
gaps outer 2
# smart_gaps on
# smart_borders on

##################
# i3 gaps change #
##################

set $mode_gaps Gaps: (o) outer, (i) inner
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
bindsym $mod+Shift+g mode "$mode_gaps"

mode "$mode_gaps" {
        bindsym o      mode "$mode_gaps_outer"
        bindsym i      mode "$mode_gaps_inner"
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

mode "$mode_gaps_inner" {
        bindsym plus  gaps inner current plus 5
        bindsym minus gaps inner current minus 5
        bindsym 0     gaps inner current set 0

        bindsym Shift+plus  gaps inner all plus 5
        bindsym Shift+minus gaps inner all minus 5
        bindsym Shift+0     gaps inner all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_outer" {
        bindsym plus  gaps outer current plus 5
        bindsym minus gaps outer current minus 5
        bindsym 0     gaps outer current set 0

        bindsym Shift+plus  gaps outer all plus 5
        bindsym Shift+minus gaps outer all minus 5
        bindsym Shift+0     gaps outer all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}

#################
# picom of i3wm #
#################

# if you want transparency on non-focused windows, ...
exec_always --no-startup-id picom --config ~/.config/i3/picom.conf

bindsym control+mod1+o exec --no-startup-id ~/.config/i3/scripts/picom-toggle.sh

####################
# START THEMING WM #
####################

# Set colors from Xresources
# Change 'color7' and 'color2' to whatever colors you want i3 to use 
# from the generated scheme.
# NOTE: The '#f0f0f0' in the lines below is the color i3 will use if
# it fails to get colors from Xresources.
set_from_resource $fg i3wm.color7 #f0f0f0
set_from_resource $fgtext i3wm.color7 #f0f0f0
set_from_resource $bg i3wm.color0 #f0f0f0
set_from_resource $gray i3wm.color2 #f0f0f0
set_from_resource $alert i3wm.color6 #f0f0f0

title_align center

# class                 bord.   bg.     text    ind.  child_border
client.focused          $fg     $bg     $fgtext $fg   $fg
client.focused_inactive $bg     $bg     $gray   $bg   $bg
client.unfocused        $bg     $bg     $gray   $bg   $bg
client.urgent           $alert  $bg     $alert  $gray $gray
client.placeholder      $fg     $bg     $fgtext $gray $gray

client.background       $bg

###################
# STOP THEMING WM #
###################

## Run pywal to set wallpaper and colors
exec_always --no-startup-id ~/.config/i3/scripts/pywal.sh ~/.wallpapers &

# Polybar ( comment out the bar settings section)
exec_always --no-startup-id ~/.config/polybar/launch.sh &

###########
# THE END #
###########