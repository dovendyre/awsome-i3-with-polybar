#!/bin/bash

####################
##### Functions ####
####################

install_software () {
    ### Installing som extra software
    programs=()
    programs+=("python-pywal")
    programs+=("seahorse")
    programs+=("rofi")
    programs+=("polybar")
    programs+=("cmake")
    programs+=("light")
    programs+=("pamac")
    programs+=("xfce4-taskmanager")
    programs+=("xfce4-appfinder")
    programs+=("sysstat")
    programs+=("acpi")
    programs+=("arcolinux-meta-fun")
    programs+=("archlinux-logout-git")
    programs+=("archlinux-logout-themes-git")
    programs+=("qt5-multimedia")
    programs+=("nemo")
    programs+=("nemo-fileroller")
    programs+=("nemo-image-converter")
    programs+=("nemo-preview")
    programs+=("nemo-seahorse")
    programs+=("duf")
    programs+=("vivaldi")
    programs+=("kitty")
    programs+=("vscodium")
    programs+=("nomacs")
    programs+=("vlc")
    programs+=("meld")
    programs+=("catdoc")
    programs+=("odt2txt")
    programs+=("autotiling")
    programs+=("scrot")
    programs+=("dex")
    programs+=("dunst")
    programs+=("flameshot")
    programs+=("paru")
    programs+=("ttf-roboto-mono-nerd")
    programs+=("ttf-cousine-nerd")
    programs+=("ttf-fantasque-nerd")

    ## Installing software
    for program in "${programs[@]}"
    do
        sudo pacman --needed --noconfirm -S ${program}
    done

    ### Removes the system picom package, new one will be installed from AUR
        sudo pacman --noconfirm -R picom


    ## Make a variable with all aur programs to install
    aurprograms=()
    aurprograms+=("picom-ibhagwan-git")
    aurprograms+=("jasper-gtk-theme-git")
    aurprograms+=("kora-icon-theme")
    aurprograms+=("graphite-cursor-theme-git")

    ### Installing som extra software from the AUR repo
    for aurprogram in "${aurprograms[@]}"
    do
        if pacman -Qs ${aurprogram} > /dev/null ; then
            echo "Package ${aurprogram} installed, skipping"
        else
            paru --noconfirm -S ${aurprogram}
        fi
    done

    ## Configure nemo file manager
    gsettings set org.cinnamon.desktop.default-applications.terminal exec kitty
    gsettings set org.nemo.preferences default-folder-viewer 'list-view'
    gsettings set org.nemo.list-view default-zoom-level standard
    gsettings set org.nemo.preferences show-home-icon-toolbar true
    gsettings set org.nemo.preferences show-new-folder-icon-toolbar true
    gsettings set org.nemo.window-state start-with-menu-bar false

}

install_acrolinux_repo () {
            ## Adding mirrors manually (had some trouble with the script earlyer)
            sudo cp $scriptdir/config/arcolinux-mirrorlist /etc/pacman.d/

            ## Adding acrolinux repo
            mkdir $scriptdir/arcotmp && git clone https://github.com/arcolinux/arcolinux-spices $scriptdir/arcotmp && sh $scriptdir/arcotmp/usr/share/arcolinux-spices/scripts/get-the-keys-and-repos.sh && rm -rf $scriptdir/arcotmp

            ## Refreshing pacman...
            sudo pacman -Syy
            
            ## Removing conflicting package
            sudo pacman --noconfirm -R i3lock
}

install_chaotic_repo () {

    ## Add Chaotic-aur repos
    sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
    sudo pacman-key --lsign-key FBA220DFC880C036
    sudo pacman --noconfirm -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'

    sudo echo "\n [chaotic-aur] \n Include = /etc/pacman.d/chaotic-mirrorlist" >> /etc/pacman.conf

}

install_zen () {
    ## Installing zen kernel
    sudo pacman --needed --noconfirm -S linux-zen linux-zen-headers

    while true; do
    echo " "
    echo "#######################################"
    read -p "Having a NVIDIA Graphics card (y/n) " yn
    echo " "
    echo "#######################################"

    case $yn in 
        [jJyY] ) echo ok, installing driver;
            ## Installing Nvidia-dkms for the zen kernal
            sudo pacman --needed -S --noconfirm nvidia-dkms
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

    done
}


sleep_lock () {
    ## Makes computer lock with suspend or lid close
    [ ! -f /etc/systemd/system/i3lock.service ] && sudo cp -f -R $scriptdir/systemd/i3lock.service /etc/systemd/system/

    nonerootuser=$USER
    sudo sed -i "/User=/c\User=$nonerootuser" /etc/systemd/system/i3lock.service
    sudo sed -i "/ExecStart=/c\ExecStart=/home/$nonerootuser/.config/dovenscripts/lock.sh" /etc/systemd/system/i3lock.service
    sudo systemctl enable i3lock.service
}

install_office () {
    sudo pacman --needed -S --noconfirm libreoffice-fresh libreoffice-fresh-nb
}

install_discord () {
    sudo pacman --needed -S --noconfirm discord
}

install_teamspeak () {
    sudo pacman --needed -S --noconfirm teamspeak3    
    sudo sed -i "/Icon=/c\Icon=teamspeak3" /usr/share/applications/teamspeak3.desktop
}

install_lutris () {
    sudo pacman --needed -S --noconfirm wine-staging winetricks
    sudo pacman -S --asdeps --needed --noconfirm  giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo libxcomposite lib32-libxcomposite libxinerama lib32-libxinerama opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader sdl2 lib32-sdl2 vkd3d lib32-vkd3d sane libgphoto2 gsm ffmpeg
    sudo pacman --needed -S --noconfirm lutris
}

install_gwe () {
    sudo pacman --needed -S --noconfirm gwe
    sudo nvidia-xconfig --cool-bits=8
}

install_corsair () {
    sudo pacman --needed -S --noconfirm ckb-next-git
}

install_asus_rog14 () {
    if pacman -Qs asusctl > /dev/null ; then
        echo "Package asusctl installed, skipping"
    else
        paru -S --noconfirm asusctl
    fi
    sed -i "/#bindsym XF86TouchpadToggle/c\bindsym XF86TouchpadToggle exec ~/.config/dovenscripts/toggletouchpad.sh # toggle touchpad" ~/.config/i3/config
    sed -i "/#bindsym XF86KbdBrightnessUp/c\bindsym XF86KbdBrightnessUp exec --no-startup-id asusctl -n # increase keyboard light" ~/.config/i3/config
    sed -i "/#bindsym XF86KbdBrightnessDown/c\bindsym XF86KbdBrightnessDown exec --no-startup-id asusctl -p # decrease keyboard light" ~/.config/i3/config
}

install_nextcloud () {
    sudo pacman --needed -S --noconfirm nextcloud-client
}

install_awsome_zsh () {
    mkdir $scriptdir/tmp && git clone https://gitlab.com/dovendyre/awsome-zsh-with-neovim.git $scriptdir/tmp && sh $scriptdir/tmp/doven_install.sh && rm -rf $scriptdir/tmp 
}

echo " "    
echo "#############################################################"    
echo "This will install my awsome-i3-with-polybar on your computer "    
echo " "    
echo "Please follow instructions... Use at own risk..... "    
echo " "    
echo "#############################################################"

### Henter ut i hvilken mappe scriptet ligger
scriptdir=$(dirname $(readlink -f $0))

## Refreshing pacman...
sudo pacman -Syy

## Queuing what to do...
to_install=()

## Add zen kernel if using Arcolinux
if grep -q Arcolinux /etc/lsb-release
then
    to_install+=("install_zen")
fi

## Add arcolinux repos
if grep -q arcolinux /etc/pacman.conf
then
    echo "Acrolinux repos allredy added!"
else
    to_install+=("install_acrolinux_repo")       
fi

## Add Chaotic-AUR repos
if grep -q chaotic-aur /etc/pacman.conf
then
    echo "Chaotic repos allredy added!"
else
    to_install+=("install_chaotic_repo")       
fi

## Installing mandatory software
to_install+=("install_software")

## Lock screen when system goes to sleep?
while true; do
    echo " "
    echo    "#####################################################################"
    read -p "Lock the screen when system goes to sleep? (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, fixing...;
            to_install+=("sleep_lock")
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

done

## Install Office?
while true; do
    echo " "
    echo    "################################## "
    read -p "Want to install Libreoffice? (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, fixing...;
            to_install+=("install_office")
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

done

## Install Discord?
while true; do
    echo " "
    echo    "############################## "
    read -p "Want to install Discord? (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, fixing...;
            to_install+=("install_discord")
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

done

## Install TeamSpeak?
while true; do
    echo " "
    echo    "################################ "
    read -p "Want to install TeamSpeak? (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, fixing...;
            to_install+=("install_teamspeak")
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

done

## Install Lutris and Wine?
while true; do
    echo " "
    echo    "###################################### "
    read -p "Want to install Lutris and Wine? (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, fixing...;
            to_install+=("install_lutris")
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

done

## Enable overclock (ONLY IF YOU HAVE NVIDIA!!) ?
while true; do
    echo " "
    echo    "################################################### "
    read -p "Install GreenWithEnvy GPU overclock software? (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, fixing...;
            to_install+=("install_gwe")
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

done

## Install corsair software?
while true; do
    echo " "
    echo    "######################################## "
    read -p "Having a Corsair keybord or mouse? (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, fixing...;
            to_install+=("install_corsair")
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

done

## Some special config for my Asus ROG G14 Laptop
while true; do
    echo " "
    echo    "################################## "
    read -p "Using a Asus Rog G14 laptop? (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, fixing...;
            to_install+=("install_asus_rog14")
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

done

## Install nextcloud sync client?
while true; do
    echo " "
    echo    "########################################## "
    read -p "Install nextcloud desktop synk tool? (y/n) " yn

    case $yn in 
        [jJyY] ) echo ok, fixing...;
            to_install+=("install_nextcloud")
            break;;
        [nN] ) echo skipping...;
            break;;
        * ) echo invalid response;;
    esac

done

# ZSH mandatory, installing awsome-zsh-with-neovim

    to_install+=("install_awsome_zsh")

## Changing look and feel
[ -f ~/.config/gtk-3.0/settings.ini ] && mv ~/.config/gtk-3.0/settings.ini ~/.config/gtk-3.0/settings.ini_org

## Want light theme? (Default theme is black)
while true; do
    echo " "
    echo    "###################################### "
    read -p "Want light theme? (y/n) " yn

    case $yn in 
        [jJyY] ) echo Enabling light theme;
            cp -f $scriptdir/gtk/light-settings.ini ~/.config/gtk-3.0/settings.ini
            break;;
        [nN] ) echo Enabling dark theme;
            cp -f $scriptdir/gtk/dark-settings.ini ~/.config/gtk-3.0/settings.ini
            gsettings set org.gnome.desktop.interface color-scheme prefer-dark
            break;;
        * ) echo invalid response;;
    esac

done

## If picom, conky, dunst or i3 config exists, backup them
[ -f ~/.config/i3/config ] && [ ! -f ~/.config/i3/config_org ] && mv ~/.config/i3/config ~/.config/i3/config_org
[ -d ~/.config/conky ] && [ ! -d ~/.config/conky_org ] && mv ~/.config/conky ~/.config/conky_org
[ -f ~/.config/i3/picom.conf ] && [ ! -f ~/.config/i3/picom.conf_org ] && mv ~/.config/i3/picom.conf ~/.config/i3/picom.conf_org
[ -f ~/.config/i3/scripts/picom-toggle.sh ] && [ ! -f ~/.config/i3/scripts/picom-toggle.sh_org ] && mv ~/.config/i3/scripts/picom-toggle.sh ~/.config/i3/scripts/picom-toggle.sh_org
[ -f ~/.config/i3/scripts/conky-toggle.sh ] && [ ! -f ~/.config/i3/scripts/conky-toggle.sh_org ] && mv ~/.config/i3/scripts/conky-toggle.sh ~/.config/i3/scripts/conky-toggle.sh_org
[ -d ~/.config/dunst ] && [ ! -d ~/.config/dunst_org ] && mv ~/.config/dunst ~/.config/dunst_org

## Backups existing scripts, i3blocks and arcologout
[ -d ~/.config/archlinux-logout ] && [ ! -d ~/.config/archlinux-logout_org ] && mv ~/.config/archlinux-logout ~/.config/archlinux-logout_org
[ -d ~/.config/polybar ] && [ ! -d ~/.config/polybar_org ] && mv ~/.config/polybar ~/.config/polybar_org
[ -d ~/.config/jgmenu ] && [ ! -d ~/.config/jgmenu_org ] && mv ~/.config/jgmenu ~/.config/jgmenu_org

## Replace the mimeapps.list to set default apps 
cp -f -R $scriptdir/mime/* ~/.config/

## "Ads new conky config"
cp -f -R $scriptdir/conky ~/.config/

## Adding jgmenu config
cp -f -R $scriptdir/jgmenu ~/.config/

## Adding dunst config
cp -f -R $scriptdir/dunst ~/.config/

## "Ads new picom and i3 config"
cp -f -R $scriptdir/i3/* ~/.config/i3/

## Ads new arcologout config
cp -f -R $scriptdir/archlinux-logout ~/.config/

## Ads some new script folder and put some scripts in it
[ ! -d ~/.config/dovenscripts ] && mkdir ~/.config/dovenscripts
cp -f -R $scriptdir/script/* ~/.config/dovenscripts/

## Ads new polybar config
cp -f -R $scriptdir/polybar ~/.config/

## Checks if rofi exists and backups the existing
[ -d ~/.config/rofi ] && [ ! -d ~/.config/rofi_org ] && mv ~/.config/rofi ~/.config/rofi_org

## Ads the new rofi config
cp -f -R $scriptdir/rofi ~/.config/

## Ads pywal colors to zsh setup
cp -f -R $scriptdir/config/.zshrc-pywal ~/

## Change text to correct keybinding on arcologout
#[ -f /usr/share/arcologout/GUI.py ] && sudo sed -i 's/(L)/(O)/g' /usr/share/arcologout/GUI.py
#[ -f /usr/share/arcologout/GUI.py ] && sudo sed -i 's/(K)/(L)/g' /usr/share/arcologout/GUI.py

## Backup the original kitty config
[ -f ~/.config/kitty/kitty.conf ] && [ ! -f ~/.config/kitty/kitty.conf_org ] && mv ~/.config/kitty/kitty.conf ~/.config/kitty/kitty.conf_org

## Updateing the kitty config
[ ! -d ~/.config/kitty ] && mkdir ~/.config/kitty
cp -f -R $scriptdir/kitty/* ~/.config/kitty/

## Makes users icon and themes folder
[ ! -d ~/.icons ] && mkdir ~/.icons
[ ! -d ~/.themes ] && mkdir ~/.themes

## Adds the mouse pointer as default cursor theme
cp -f -R $scriptdir/config/default ~/.icons/

## Adds cursor to the login-screen
sudo sed -i -e "s/cursor-theme-name=.*/cursor-theme-name=Graphite-dark-cursors/g" /etc/lightdm/slick-greeter.conf

### Skipping done with AUR package ######################
## Add the new themes icons and pointers                #
#sudo cp -f -R $scriptdir/icons/* /usr/share/icons/     #
#sudo cp -f -R $scriptdir/themes/* /usr/share/themes/   #
## Mouse cursor for the login screen                    #
#sudo cp -f -R $scriptdir/mouse/* /usr/share/icons/     #
#########################################################

## Font made global
sudo cp -f -R $scriptdir/fonts/* /usr/share/fonts/

## Adding wallpapers to home dir
[ ! -d ~/.wallpapers ] && mkdir ~/.wallpapers
cp -f -R /$scriptdir/wallpapers/* ~/.wallpapers/


for install_this in "${to_install[@]}"
do
    $install_this
done



## Remove some startup applications..
[ -d ~/.config/autostart ] && [ ! -d ~/.config/autostart_org ] && mv ~/.config/autostart ~/.config/autostart_org
cp -f -R $scriptdir/autostart ~/.config/

## Sets a new random background and set colors
sh ~/.config/i3/scripts/pywal.sh ~/.wallpapers &

echo " "
echo " "
echo " ###################################################################"
echo " You need to set a lockscreen image and close the window once done.."
echo " ###################################################################"

[ -f /bin/archlinux-betterlockscreen ] && /bin/archlinux-betterlockscreen &

echo " "
echo " "
echo " #########################################"
echo " Adjust power settings as you want them..."
echo " #########################################"

[ -f /bin/xfce4-power-manager-settings ] && /bin/xfce4-power-manager-settings &

echo " "
echo " "
echo " ######### "
echo " D O N E ! "
echo " ######### "
echo " "
echo " "
echo " ###################################################################################"
echo " To disable locking screen at suspend run command: systemctl disable i3lock.service "
echo " ###########################################################################################"
echo " Add your favourute wallpapers to the ~/.wallpapers directory. Will be randomized at login. "
echo " ###########################################################################################"
echo " "
echo " Please reboot for all changes to take effect! "